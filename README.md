# Graphql Chat

Um chat simples feito com Apollo GraphQL, utilizando Node.js e React.


# Instruções:

A aplicação precisa do servidor rodando, então, siga o passo a passo em ordem
deixe um rodando e em seguida execute o outro.

### Servidor

- No terminal, entre na pasta 'server' e execute:
`yarn install` ou `npm install`
- Para rodar local observando as alterações execute:
`yarn start` ou `npm run start`


### Aplicação

- No terminal, entre na pasta 'frontend' e execute:
`yarn install` ou `npm install`
- Para rodar local observando as alterações execute:
`yarn start` ou `npm run start`


